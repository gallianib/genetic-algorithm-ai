#include <stdio.h>

#include <stdlib.h>


#include <time.h>
#include "ReadFile.h"
/////////////////////////////////
/////////////////////////////////
/////////////////////////////////
/////////////////////////////////

void print_array(int x,int y, int **arr);

void CrossOver(int *p1, int *p2,int x,int *c);

int** make_array(int x, int y);

void scan_file(FILE *fp, int **arr,int x,int y);

void free_array(int **arr,int x);

void create_makespan(int **arr,int** makespan, int x, int y,int *score,int *makeOrder);

int*** make_3darray(int x, int y,int pop);

void print_mksn(int y,int x, int **arr);

void shuffle(int *array, int n);

void free_3darray(int ***arr, int pop, int x);

int getMinScore(int *array,int y);

void writeOutput(char *filen, int x, int y, int *score, int *order, int **make);
void Mutate(int *array, int n,int* m,int muts);

void computeFitness(int *scorelist,int Finalscore, int x, int pop, int muts,int **makeOrderList,int** muteOrderList,int* madeOrderList);
/////////////////////////////////
/////////////////////////////////
/////////////////////////////////
/////////////////////////////////
int** read_data(FILE* fp,int* xp,int* yp){
	fscanf(fp,"%d %d",xp,yp);
	int **arr = make_array(*xp,*yp);
	scan_file(fp,arr,*xp,*yp);
	printf("x: %d, y: %d",*xp,*yp);
	print_array(*xp,*yp,arr);
	return arr;
}
/////////////////////////////////
/////////////////////////////////
/////////////////////////////////
//FUNCTION:	Main
/////////////////////////////////
int main(int argc, char* argv[])
{

	time_t t;
	int mode = 0;
	 
	srand((unsigned) time(&t));
    if (argc > 1 && argc < 4) {
        
   	printf("Usage : InputFile : OutputFile : RunTime(seconds)\n");
 
        return 1;

    }
    else if (argc == 1) {
        
    	mode = 1;
 	
    }

    
  
/////////////////////////////////////INITIALIZATIONS///////////////////////////
	FILE *fp;
	clock_t amount_t =(double)(10 * 1000);;
	char *ofn = "output.txt";;
	  if (mode== 1){
	    amount_t = (double)(10 * 1000);
  	    fp = fopen("test.txt","r");
	    char *ofn = "output.txt";
	  }else{
	    amount_t = (double)(atoi(argv[3])* 1000);
  	    fp = fopen(argv[1],"r");
	    ofn = argv[2];
	  }
 	  if( fp == NULL ){
 	     perror("Error opening the file.\n");
 	     exit(EXIT_FAILURE);
 	  }else {
	    printf("will run for %ld seconds\n",amount_t);
 	    int x,y,i,j;
	    int score = 0;
	    int *scorep = &score;
	    int *xp = &x;
	    int *yp = &y; 
	    int pop = 15000;/////////////////////POPULATION SET HERE/////////////////////
	    int maxMuts = pop;/////////////////SET MAX # OF MUTS HERE//////////////////////
	    double mutationRate = 0.5;/////////////////MUTATION RATE SET HERE///////////////////////
	    double crossOverRate = 0.6;/////////////////CrossOver RATE SET HERE///////////////////////
	    int maxPop = pop+maxMuts;
	    int generationNumber = 0;//////////////////////NUMBER OF GENERATIONS SET HERE//////////////////
	    int **arr = read_data(fp,xp,yp);//////////////////////DATA READ HERE/////////////////
	    int **makeOrderList = make_array(pop,x);
	    int **muteOrderList = make_array(pop,x);
	    int madeOrderList[x];
	    int scorelist[maxPop];
	    int Finalscore;
	    int makeOrder[x];
	    int muts = 0;
	    int** makespan = make_array(y,x);
printf("\nstarting\n");
	    for(i = 0;i < x; i++){
		makeOrder[i] = i;
	    }///////////////////////////////////CREATING INITIAL MAKESPANS/////////////////////

	    for(i = 0;i < pop;i++){
		shuffle(makeOrder,x);
	    	create_makespan(arr,makespan,x,y,scorep,makeOrder);
		scorelist[i] = *scorep;
		Finalscore = *scorep;
		for(j = 0;j < x; j++){
			makeOrderList[i][j] = makeOrder[j];
		}
		
		
	    }

		int bestfit = 0;
clock_t current_t = clock();
	do{ //////////////////////DO-WHILE LOOP STARTS HERE///////////////////
		//if(generationNumber > 0){
		//	makeOrderList[0] = madeOrderList;
		//}
		muts = 0;
		double radiator = 0;
		double cross = 0;
		int hit=0; 
		int chit=0; 
		int anotherOther = 0;
		int specialOther = 0;
	if(generationNumber % 15 == 0 && generationNumber >= 0){
	  		int m = pop/2;
			//int m = (rand() % pop)+1;
			//int m = (pop / 2) + (pop/4);
			for(i = (pop-1);i>1;i--){
				if(i > m){
					specialOther = rand() %  x;
					shuffle(makeOrderList[i],specialOther);
				}else {
					Mutate(makeOrderList[i],x,muteOrderList[muts],muts);
					CrossOver(makeOrderList[0],makeOrderList[i],x,muteOrderList[muts]);
				}
				create_makespan(arr,makespan,x,y,scorep,makeOrderList[i]);
				scorelist[i] = *scorep;
			} 
	}else{
printf("\n");
	      for(i = 1;i < pop;i++){
		hit = 0;
		chit = 0;
		//printf("%d ",scorelist[i]);
		radiator = (double)rand() / (double)RAND_MAX;
		cross = (double)rand() / (double)RAND_MAX;
		specialOther = rand() %  pop;
		anotherOther = rand() %  pop;
		while(specialOther == anotherOther){specialOther = rand()%pop;}
		 if(radiator < mutationRate && generationNumber > 0){
			Mutate(makeOrderList[i],x,muteOrderList[muts],muts);
		   
			hit = 1;
			
		}
		if(cross < crossOverRate && generationNumber > 0){
			CrossOver(makeOrderList[anotherOther],makeOrderList[specialOther],x,muteOrderList[muts]);
			chit = 1;
		}
		if(hit == 1 || chit == 1){
			create_makespan(arr,makespan,x,y,scorep,muteOrderList[muts]);
			scorelist[pop+muts] = *scorep;
			
			if(muts+1 < pop)muts++;
		}
	      }
	}
		printf("\n\nselection for gen#: %d",generationNumber);
		printf("\nMuts:%d\nMake Order: ",muts);
		computeFitness(scorelist,Finalscore,x,pop,muts,makeOrderList, muteOrderList,madeOrderList);
    		bestfit = Finalscore;
		create_makespan(arr,makespan,x,y,scorep,madeOrderList);
		for(i=0;i<x;i++){
			printf("%d ",madeOrderList[i]);
		}
		printf("\n");
		
		printf("BestFit Scored: %d",*scorep);

		generationNumber++;
current_t = clock();

	    }while(current_t < amount_t);//////**NUMBER OF GENERATIONS HERE**///////////DO-While ENDS HERE////////////
		
		bestfit = Finalscore;
		create_makespan(arr,makespan,x,y,scorep,madeOrderList);
		printf("\nWriting output with: \n Score: %d\n Make Order: ", *scorep);
		for(i=0;i<x;i++){
			printf("%d ",madeOrderList[i]);
		}
		printf("\n");
		
		printf("BestFit Scored: %d",*scorep);
    		writeOutput(ofn,x,y,scorep,madeOrderList,makespan);

  	    fclose(fp);

	    free_array(arr,x);

	    free_array(makespan,y);
	    printf("\nclosing\n");
	    free_array(muteOrderList,pop);
		printf("\nclosing\n");
	    free_array(makeOrderList,pop);

	    printf("closed.\n");
	}
     //}
   return 0;
}
/////////////////////////////////
/////////////////////////////////
/////////////////////////////////
//FUNCTION: CrossOver --	    Applies CrossOver to two makeOrders
/////////////////////////////////

void CrossOver(int *p1, int *p2,int x,int *c) {    
	int e = (rand() % x);
	if(e == 0)e++;
	if(e == x)e--;
	int s = rand() % e;
	if(s==e)s--;
	int i;
	int ccount = 0;
	for(i = 0;i<x;i++){
		c[i] = p1[i];
	}

	int j;
	int trig = 0;
	for(i = 0;i<x;i++){
		trig = 0;
	    for(j = s;j<=e;j++){
		if(p2[i] == c[j]){
		    trig = 1;   	
		}
	    }
	    if(trig == 0){
		if(ccount == s){
			ccount = e+1;
		}
	        c[ccount] = p2[i];
		
		if(ccount < x)ccount++;
	    }
	}
}

/////////////////////////////////
/////////////////////////////////
/////////////////////////////////
//FUNCTION: Mutate --	    Applies Mutations to the makeOrder
/////////////////////////////////

void Mutate(int *array, int n,int* m,int muts) { 
  
	int i = 0; 
	
	for(i=0;i<n;i++){
		m[i] = array[i];
	}
    if (n > 1) {	
	int e = (rand() % n)+1;
	if(e >= n)e = n-1;
	int s = rand() % e;
	int t = m[s+1];
	m[s+1] = m[e];
	s++;
	for(i=s;i<e;i++){
		int t2 = m[i+1];
		m[i+1] = t;
		t = t2;
	}
	
    }

}

/////////////////////////////////
/////////////////////////////////
/////////////////////////////////
//FUNCTION:	Computes Fitness values
/////////////////////////////////
void computeFitness(int *scorelist,int Finalscore, int x, int pop, int muts,int **makeOrderList,int** muteOrderList,int* madeOrderList){
	int i;
	int r;
	int j;
	double p; 
	double n;
	int chosen = 0;
	int low = getMinScore(scorelist,pop+muts);
	int min = scorelist[low];
	if(min < Finalscore){
		if(low>=pop){
			for(j=0;j<x;j++){
				madeOrderList[j] = muteOrderList[low-pop][j];
				makeOrderList[0][j] = muteOrderList[low-pop][j];
				}
		}else if(low < pop){
			for(j=0;j<x;j++){
				madeOrderList[j] = makeOrderList[low][j];
				makeOrderList[0][j] = makeOrderList[low][j];
				}
		}

		Finalscore = scorelist[low];

	for(i = 0; i < pop; i++){
		for(j = 1; j < x; j++){
			makeOrderList[i][j] = madeOrderList[j];
		}
		scorelist[i] = scorelist[low];
	}
	for(i = pop; i < pop+muts; i++){
		scorelist[i] = scorelist[low];
	}		
	}
}

/////////////////////////////////
/////////////////////////////////
/////////////////////////////////
//FUNCTION: create_makespan --	    Creates and returns a single solution
/////////////////////////////////


void create_makespan(int **arr,int** makespan, int x, int y,int *score,int *makeOrder){
	int i = 0;
	int j = 0;
	int z = 0;
	int up = 0;
	int left = 0;
	int ct = 0;
	makespan[0][0] = 0;
	for(j = 1;j < x;j++){
		z = makeOrder[ct];
		makespan[0][j] =  makespan[0][j-1] + arr[z][0];
		ct++;
	}
	ct = 0;
	
	for(i = 1;i < y;i++){
		z = makeOrder[0];
		makespan[i][0] = makespan[i-1][0] + arr[z][i-1];
	  for(j = 1;j < x;j++){
		z = makeOrder[j];
		up = makespan[i-1][j]+ arr[z][i-1]; 
		left = makespan[i][j-1]+ arr[(makeOrder[(j-1)])][i]; 
		if(up<left){
			makespan[i][j] = left;
		}else makespan[i][j] = up; 
	  }
	}
	z = makeOrder[x-1];	
	*score = makespan[y-1][x-1]+arr[z][y-1];

}
/////////////////////////////////
/////////////////////////////////
/////////////////////////////////
//FUNCTION: writeOutput --	Writes to file(makespan,and the best fit solution)
/////////////////////////////////


void writeOutput(char *filen, int x, int y, int *score, int *order, int **make){
	FILE *file;
	file = fopen(filen, "w");
	fprintf(file, "%s\n%d\n","makespan",*score);
	
	int i,j;
	for(i = 0; i < y;i++){
	  for(j = 0; j < x;j++){
		fprintf(file, "%d %d ",order[j],make[i][j]);
	  }
		fprintf(file,"\n");
	}
	fclose(file);

}
/////////////////////////////////
/////////////////////////////////
/////////////////////////////////
//FUNCTION: free_array --	Frees a 2d array
/////////////////////////////////


void free_array(int **arr,int x){
	int i;

	for(i = 0;i < x;i++){
	        if (arr[i] != NULL) {
			free(arr[i]);
		}
	}
	free(arr);

}
/////////////////////////////////
/////////////////////////////////
/////////////////////////////////
//FUNCTION: free_3darray --	Frees a 3d array
/////////////////////////////////


void free_3darray(int ***arr, int pop, int x){
    int i, j;

    for (i=0; i < pop; i++) {
        if (arr[i] != NULL) {
            for (j=0; j < x; j++){
                free(arr[i][j]);
	    }
            free(arr[i]);
        }
    }
    free(arr);
}
/////////////////////////////////
/////////////////////////////////
/////////////////////////////////
//FUNCTION: scan_file --	Reads data from file
/////////////////////////////////


void scan_file(FILE *fp, int **arr,int x, int y){
        int temp,i,j;
	for(i = 0; i < x; i++){
		for(j = 0; j < y; j++){
			fscanf(fp,"%d",&temp);
			fscanf(fp,"%d",&arr[i][j]);
		}
	}
}
/////////////////////////////////
/////////////////////////////////
/////////////////////////////////
//FUNCTION: getMinScore --	Returns lowest int in array
/////////////////////////////////


int getMinScore(int *array,int y){
	int i,p = 0;
	int t = array[0];
	for(i = 1; i < y; i++){
		if(array[i] < t){
			t = array[i];
			p = i;
		}
	}
	return p;
}
/////////////////////////////////
/////////////////////////////////
/////////////////////////////////
//FUNCTION: print_array --	prints a 2d array to console
/////////////////////////////////


void print_array(int x,int y, int **arr){
		printf("\n");
	for(int i = 0;i < x;i++){
		for(int j = 0;j < y;j++){
		  if(arr[i][j]>9){
			printf(" %d",arr[i][j]);
		  }else printf("  %d",arr[i][j]);
		}
		printf("\n");
	}
		printf("\n");
}
/////////////////////////////////
/////////////////////////////////
/////////////////////////////////
//FUNCTION: make_array --	malloc 2d array(X:Y)
/////////////////////////////////


int** make_array(int x, int y){
	int **arr;
	int i;
	arr = (int **)malloc(sizeof(int *) * x);

	for(i = 0; i < x; i++){
		arr[i] = (int *)malloc(sizeof(int) * y);
	}

	return arr;
}
/////////////////////////////////
/////////////////////////////////
/////////////////////////////////
//FUNCTION: make_3darray --	malloc 3d array (POP:X:Y)
/////////////////////////////////


int*** make_3darray(int x, int y,int pop){
	int i,j;
	int ***arr = (int ***)malloc(sizeof(int **) * pop);
	for(j = 0; j < pop;j++){
		arr[j] = (int **)malloc(sizeof(int *) * x);
		for(i = 0; i < x; i++){
			arr[j][i] = (int *)malloc(sizeof(int) * y);
		}
	}

	return arr;
}
/////////////////////////////////
/////////////////////////////////
/////////////////////////////////
//FUNCTION: shuffle --	Scrables the order of int in an array
/////////////////////////////////

void shuffle(int *array, int n) {    
    if (n > 1) {
        int i;
        for (i = n - 1; i > 0; i--) {
            int j = (unsigned int) (rand() % (i+1));
            int t = array[j];
            array[j] = array[i];
            array[i] = t;   
	}
    }
}                    